# Entry-Logger-in-java



import java.io.*;
import org.apache.bcel.*;
import org.apache.bcel.classfile.*;
import org.apache.bcel.generic.*;

public class EntryLogger
{

   private ClassGen cg;
   private ConstantsPoolGen cpg;
   
   public static void main(String[] args)
   {
      try
      { 
         if(args.length == 0)
          System.out.println("Usage java bytecodeAnnotations.EntryLogger classname");
          else
          {
             JavaClass jc - Repository.lookupClass(args[0]);
             ClassGen cg = new ClassGen(jc);
             EntryLogger el = new EntryLogger(cg);
             el.convert();
             String f = Repository.lookupClassFile(cg.getClassName()).getPath();
             System.out.println("Dumping " + f);
             cg.getJavaClass().dump(f);
          }
       }
       catch (Exception e)
       {
           e.printStackTrace();
       }
    }
    
    public EntryLogger(ClassGen cg)
    {
        this.cg = cg;
        cpg = cp.getConstantPool();
    }
    
    public void convert() throws IOException
    {
       for (Method m : cg.getMethods())
       {
            AnnotationEntry[] annotations = m.getAnnotationEntries();
            for (AnnotationEntry a : annotations)
            {
                if (a.getAnnotationType().equals("LbytecodeAnnotations/LogEntry;"))
                {
                   for (ElementValuePair p : a.getElementValuePairs())
                   {
                         if (p.getNameString().equals("logger"))
                         {
                             String loggerName = p.getValue().stringfyValue();
                             cg.replaceMethod(m, insertLogEntry(m, loggerName));
                             
                         }
                       }
                    }
                 }
             }
        }
    }
   
   private Method insertLogEntry(Method m, String loggerName)
   {
       Method mg = new MethodGen(m, cg.getClassName(), cpg);
       String className = cg.getClassName();
       String methodName = mp.getMethod().getName();
       System.out.printf("Adding logging instructions to %s.%s%n className, methodName");
       
       int getLoggerIndex = cpg.addMethodref("java.util.loggingLogger", "getLogger", "(Ljava/lang/String;)Ljava/util/logging/Logger;);
       int enteringIndex = cpg.addMethodref("java.util.logging.Logger", "entering", "(Ljava/lang/String;Ljava/lang/String;)V");
       
       InstructionList il = mg.getInstructionList();
       InstructionList patch = new InstructionList();
       patch.append(new PUSH(cpg, loggerName));
       patch.append(new INVOKESTATIC(getLoggerIndex));
       patch.append(new PUSH(cpg, className));
       patch.append(new INVOKEVIRTUAL(enteringIndex));
       InstructionHandle[] ihs = il.getInstructionHandles();
       il.insert(ihs[0], patch);
       
       mg.setMaxStack();
       return mg.getMethod();
     }
  }               
                            
             
             
          
